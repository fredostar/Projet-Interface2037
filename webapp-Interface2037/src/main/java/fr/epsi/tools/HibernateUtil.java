package fr.epsi.tools;

import fr.epsi.entites.Question;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by fredericradigoy on 16/04/15.
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;


    public static void buildSesionFactory()
    {
        try
        {
            Configuration configuration = new Configuration();
            configuration.addClass(Question.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        }
        catch (HibernateException ex)
        {
            throw new RuntimeException("Problème de configuration:" +ex.getMessage(), ex);

        }


    }

    public static SessionFactory getSessionFactory() {

        if(sessionFactory == null)
            buildSesionFactory();
        return sessionFactory;
    }
}

package fr.epsi;

/**
 * Created by fredericradigoy on 16/04/15.
 */
public class InvalidQuestionException extends Exception {



    public InvalidQuestionException()
    {
        super();
    }

    public InvalidQuestionException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidQuestionException(String message)
    {
        super(message);
    }

    public InvalidQuestionException(Throwable cause)
    {
        super(cause);
    }


}

package fr.epsi.services;


import fr.epsi.entites.Question;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.sql.SQLException;

/**
 * Created by frederic radigoy on 16/04/15.
 */
public class QuestionWS {



    @POST
    @Path("/question/{param}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addQuestion(@PathParam("param") String question) throws Exception {
        Response response;
        Question laQuestion = new Question();


        try
        {
            laQuestion.save(question);
            String adresseRessource = "http://localhost:8080/interface2037/api/client/question/"+question;
            response = Response.accepted()
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Expose-Headers", "Location")
                    .header("Location", new URI(adresseRessource))
                    .build();

        }
        catch (Exception e)
        {
           throw new Exception(e.getMessage());
        }

        return response;
    }



    @PUT
    @Path("/question/{id}/{response}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getReponse(@PathParam("id") int id, @PathParam("response") String response, @Context Request request) throws SQLException
    {
        Question question = Question.findQuestionById(id);
        EntityTag entityTag = new EntityTag(Integer.toString(question.hashCode()));

        Response.ResponseBuilder builder = request.evaluatePreconditions(entityTag);

        if (builder != null)
        {
            return builder.build();
        }

        question.update(response);

        builder = Response.noContent();

        return builder.build();

    }



    @GET
    @Path("/question/{code}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getReponse(@PathParam("code") int id, @Context Request request) throws Exception {
        Response.ResponseBuilder builder = null;

        Question laQuestion = Question.findQuestionById(id);

        CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);
        cc.setPrivate(true);

        EntityTag entityTag = new EntityTag(Integer.toString(laQuestion.hashCode()));
        builder = request.evaluatePreconditions(entityTag);

        if (builder == null)
        {
            builder = Response.ok(laQuestion);
            builder.tag(entityTag);

        }

        return builder.build();
    }



}

# Projet-Interface2037

Interface 2037 est un service Web permettant à un usager de soumettre des questions à un système
expert afin d'obtenir une réponse.
Les composants du système :
  - Le serveur de questions : il mémorise les questions posées par les usagers et les réponses
fournies par les systèmes experts.
  - Le client usager : il permet à un individu de poser une question et de consulter sa réponse
une fois qu'elle a été traitée par un système expert.
  - Le client système expert : il consulte les questions une à une et fournit une réponse.

Solution trouvée pour répondre au besoin:
  - Créer des Services Web


Technologies utilisées:
  - Intellij Idea
  - Java
  - Maven
  - Hibernate
  - concordion
  - Jersey jax rs 
